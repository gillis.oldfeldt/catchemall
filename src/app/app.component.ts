import { Component } from '@angular/core';
import { Location } from '@angular/common'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Who\'s that pokemon!?';
  
  constructor(
    private location: Location
  ) { }

  onClickBack() {
    this.location.back()
  }
  onClickTop() {
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });
  }
}

