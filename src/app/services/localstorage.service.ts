import { Injectable } from '@angular/core';
import { PokeapiService } from './pokeapi.service';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor(private pokeApiService: PokeapiService) { }

  getDataAndSanitize(key:string){
    let returnVal:any = {}
    try{
      returnVal = JSON.parse(localStorage.getItem(key) || "{}")
    }
    catch(e){
      localStorage.removeItem(key)
    }
    return returnVal
  }

  getPokedex(){
    return this.getDataAndSanitize("pokedex")
  }
  getShinydex(){
    return this.getDataAndSanitize("shinydex")
  }

  getBadges(){
    return this.getDataAndSanitize("badges")
  }

  getName(){
    return this.getDataAndSanitize("trainerName")
  }

  setName(name: string){
    localStorage.setItem("trainerName", JSON.stringify(name));
  }

  getRegion(){
    return this.getDataAndSanitize("region")
  }

  setRegion(name: string){
    localStorage.setItem("region", JSON.stringify(name));
  }

  getPokemon(){
    return this.getDataAndSanitize("favPokemon")
  }

  setPokemon(name: string){
    localStorage.setItem("favPokemon", JSON.stringify(name));
  }

  setBadges(generation: number, level:number){
    const badges = this.getBadges()
    console.log(badges)
    console.log(level)
    console.log(generation)
    if(!badges[generation]||badges[generation]<level){
      localStorage.setItem("badges", JSON.stringify({...badges, [generation]:level}))
    }
  }

  setSeenPokedex(pokemon:any){
    const pokedex = this.getPokedex()
    
    localStorage.setItem("pokedex", JSON.stringify({...pokedex, [pokemon]:true}))
  }

  setSeenShinydex(pokemon:any){
    const pokedex = this.getShinydex()
    
    localStorage.setItem("shinydex", JSON.stringify({...pokedex, [pokemon]:true}))
  }

  populatePokeList(){
    this.pokeApiService.getAllPokemonNames().subscribe(pokelist => localStorage.setItem("pokelist", JSON.stringify(pokelist)))

  }
  
  getPokeList(){
    return this.getDataAndSanitize("pokelist")
  }
  
}
