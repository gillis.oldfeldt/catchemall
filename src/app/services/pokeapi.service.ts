import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, concatAll } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PokeapiService {
  baseUrl = "https://pokeapi.co/api/v2/"
  
  constructor(private http: HttpClient) { }

  getAllPokemon() {
    return this.http.get<any>(`${this.baseUrl}pokemon-species`, {params:{limit:1000}}).pipe(map(answer => answer.results))
  }

  getAllPokemonNames() {
   return this.getAllPokemon().pipe(map(pokelist => pokelist.map((pokemon: { name: string; }) => pokemon.name)))
  }

  getPokemon(pokemon:String){
    return this.http.get<any>(`${this.baseUrl}pokemon-species/${pokemon}`).pipe(map(pokemon => this.http.get<any>(pokemon.varieties[0].pokemon.url)), concatAll())
  }

  getPokemonDetails(pokemon:String){
    return this.getPokemon(pokemon).pipe(
      map( pokemon => (
          {
            "id": pokemon.id,
            "name":pokemon.name,
            "types":pokemon.types.map(
                (typeObj: { type: { name: String; }; }) => typeObj.type.name
            ),
            "height":pokemon.height,
            "weight":pokemon.weight,
            "sprite":pokemon.sprites.front_default
          
          }
        )
      )
    )
  }

  getSprite(pokemon:String){
    return this.getPokemon(pokemon).pipe(map(answer => answer.sprites.front_default))
  }

  getShinySprite(pokemon:String){
    return this.getPokemon(pokemon).pipe(map(answer => answer.sprites.front_shiny))
  }

  getAllPokemonFromGeneration(gen:string){
    return this.http.get<any>(`${this.baseUrl}generation/${gen}`).pipe(map(answer => answer.pokemon_species))
  }

  getRandomPokemon(){
    return this.getAllPokemon().pipe(
      map(
        (pokelist:Array<any>) => 
          pokelist
          .sort(() => Math.random()- 0.5)
          .sort(() => Math.random()- 0.5)
          .map(pokemon => pokemon.name)[0]
      )
    )
  }

  getRandomPokemonGen(gen:number){
   return this.getAllPokemonFromGeneration(JSON.stringify(gen)).pipe(
      map(
        (pokelist:Array<any>) => 
          pokelist
          .sort(() =>Math.random() - 0.5)
          .sort(() => Math.random()- 0.5)
          .map(pokemon => pokemon.name)[0]
      )
    )
  }

  getPokemonType(pokemon:String){
    return this.getPokemon(pokemon)
      .pipe(
        map(
          pokemon => pokemon.types.map(
            (typeObj: { type: { name: String; }; }) => typeObj.type.name
          ).join(" ")
        )
      )
  }

}
