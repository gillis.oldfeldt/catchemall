import { Component, NgModule, OnInit } from '@angular/core';
import { LocalstorageService } from 'src/app/services/localstorage.service';
import {PokeapiService} from 'src/app/services/pokeapi.service';
import { ViewportScroller } from "@angular/common";

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.css']
})
export class PokedexComponent implements OnInit {

  pokedex: any[] = [];

  pokedexFiltered: String[] = [];

  generations: string[] = ["1", "2", "3", "4", "5", "6", "7", "8"];

  constructor(
    private apiService: PokeapiService,
    private localStorage: LocalstorageService,
    private scroller: ViewportScroller
    ) 
    {}

  ngOnInit(): void {
    this.fillPokedex();
  }

  //Fetches all pokemon from the api to one list and at the same time the filters another list to only contain 
  //those the player has correctly guessed before. Correct guesses are fetched from the localstorage.
  fillPokedex(){
    this.apiService.getAllPokemon().subscribe(info => {this.pokedex = info;
      this.pokedexFiltered = this.pokedex.map(pokemon => this.localStorage.getPokedex()[pokemon.name] ? pokemon.name : false)
    } );
  }

  //Adds appropriate amount of zeros to index in list so all id consists of three numbers
  idGenerator(id:number) {
    id++;
    if (id < 10) {
      return '00' + id;
    } else if (id < 100) {
      return '0' + id;
    } else {
      return id;
    }
  }

  getNumberOfPokemonFound() {
    if (this.pokedexFiltered.length) {
      let nbr = 0;
      this.pokedexFiltered.forEach(pokemon => pokemon ? nbr++ : nbr);
      return nbr;
    } else {
      return 'Counting pokémon...'
    }
  }

  //Determines where delimiters between generations should be based on how many
  // pokemon there are in each generation
  checkGeneration(index:number) {
    switch (index) {
      case 151:
        return 2;
      case 251:
        return 3;
      case 386:
        return 4;
      case 493:
        return 5;
      case 649:
        return 6;
      case 721:
        return 7;
      case 809:
        return 8;
      default:
        return 0;
    }
  }

  onClickMove(id:string) {
    this.scroller.scrollToAnchor(id);
  }

}
