import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from 'src/app/services/localstorage.service';


@Component({
  selector: 'app-game-starter',
  templateUrl: './game-starter.component.html',
  styleUrls: ['./game-starter.component.css']
})
export class GameStarterComponent implements OnInit {

  genlist: number[] = [1,2,3,4,5,6,7,8]
  gen:number = 0
  showGame:boolean=true
  constructor(private localstorageService: LocalstorageService) { }

  ngOnInit(): void {
    this.localstorageService.populatePokeList()
  }
  selectGen( gen:number):void{
    this.gen = gen
    this.showGame=false
  }
  reloadGameSelect(){
    this.gen = 0
    this.showGame=true
    console.log("button pressed")
  }

}
