import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LocalstorageService } from 'src/app/services/localstorage.service';
@Component({
  selector: 'app-guessing',
  templateUrl: './guessing.component.html',
  styleUrls: ['./guessing.component.css']
})
export class GuessingComponent implements OnInit {

  currentIdx = 0
  nPokemon = 10
  pokemon: String[] = []
  results: Array<any> = []
  finishedGame: Boolean = false

  @Input() gen?: number
  @Output() roundFinish = new EventEmitter()
  constructor(private localstorageService: LocalstorageService) { 
  }

  ngOnInit(): void {
    this.localstorageService.populatePokeList()
  }

  nextPokemon(event:any){

    this.results.push({idx:this.currentIdx, ...event})
    this.currentIdx = (this.currentIdx+1)

    if(this.currentIdx == this.nPokemon){
      this.gameFinished()
    }
  }

  gameFinished(){
    this.finishedGame = true
    this.localstorageService.setBadges(this.gen||0,this.calculateResult())
  }

  calculateResult(){
      return this.results.map(guess => (+ guess["noTypeHint"] + guess['pokemon'])).reduce((partialSum, a) => partialSum + a, 0);
  }

  newGame(){

    this.results=[]
    this.roundFinish.emit()
    console.log("button lower")
  }
}
