import { Component, OnInit, Input } from '@angular/core';
import {PokeapiService} from 'src/app/services/pokeapi.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './pokemon-detail.component.html',
  styleUrls: ['./pokemon-detail.component.css']
})
export class PokemonDetailComponent implements OnInit {

  pokemonName: string = "";

  pokemon: any | undefined;

  constructor(
    private apiService: PokeapiService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.pokemonName = String(this.route.snapshot.paramMap.get('name'));
    this.pokemon = this.getPokemonDetails();
  }

  //Fetches information from the api about the currently chosen pokemon. 
  //Chosen pokemon is set on init as the name in the url
  getPokemonDetails() {
      if(this.pokemonName){
        this.apiService.getPokemonDetails(this.pokemonName).subscribe(pokemon => this.pokemon = pokemon);
      } else {
        console.log("Pokemon existerar inte")
      }
  }

  goBack(): void {
    this.location.back();
  }

}
