import { Component, OnInit } from '@angular/core';
import { LocalstorageService } from 'src/app/services/localstorage.service';
import {PokeapiService} from 'src/app/services/pokeapi.service';
import { Location } from '@angular/common'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  trainerName!: any;
  region!: any;
  favPokemon!: any;
  favPokemonSprite!: any;
  regions: String[] = ["Kanto", "Johto", "Hoenn", "Sinnoh", "Unova", "Kalos", "Alola", "Galar" ];
  starterPokemons: String[] = ["Bulbasaur", "Charmander", "Squirtle", "Pikachu"];

  badges!: any;

  kantoImage!: any;
  johtoImage!: any;
  hoennImage!: any;
  /* To be implemented once images has been photoshopped
  sinnohImage!: any;
  unovaImage!: any;
  kalosImage!: any;
  alolaImage!: any;
  galarImage!: any;*/

  constructor(
    private localStorage: LocalstorageService,
    private pokeapiService: PokeapiService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.trainerName = typeof this.localStorage.getName() === "string" ? this.localStorage.getName() : "";
    this.region = typeof this.localStorage.getRegion() === "string" ? this.localStorage.getRegion() : "";
    this.favPokemon = typeof this.localStorage.getPokemon() === "string" ? this.localStorage.getPokemon() : "";
    this.favPokemonSprite = this.setPokemonSprite();
    this.badges = this.localStorage.getBadges();
    this.determineBadges();
  }

  setName(){
    this.localStorage.setName(this.trainerName);
  }

  setRegion(){
    this.localStorage.setRegion(this.region);
  }

  setPokemon(){
    this.localStorage.setPokemon(this.favPokemon);
    this.favPokemonSprite = this.setPokemonSprite();
  }

  //Sets the image link to the one with correct amount of badges
  determineBadges(){
    this.kantoImage = "assets/images/kanto_badges_" + this.scoreToBadges(1) + ".png";
    this.johtoImage = "assets/images/johto_badges_" + this.scoreToBadges(2) + ".png";
    this.hoennImage = "assets/images/hoenn_badges_" + this.scoreToBadges(3) + ".png";
    /* To be implemented once images has been photoshopped
    this.sinnohImage = "assets/images/sinnoh_badges_" + this.badgeAmount(4) + ".png";
    this.unovaImage = "assets/images/unova_badges_" + this.badgeAmount(5) + ".png";
    this.kalosImage = "assets/images/kalos_badges_" + this.badgeAmount(6) + ".png";
    this.alolaImage = "assets/images/alola_badges_" + this.badgeAmount(7) + ".png";
    this.galarImage = "assets/images/galar_badges_" + this.badgeAmount(8) + ".png";*/
  }

  /*Translates the highscore in the localstorage to what amount of badges should be displayed in the profile
    Takes a generation number as input */
  scoreToBadges(gen: number){
    let score = this.badges[gen]
    if(score < 4){
      return "0"
    } else if(score < 6 ){
      return "1"
    } else if(score < 8 ){
      return "2"
    } else if(score < 10 ){
      return "3"
    } else if(score < 12 ){
      return "4"
    } else if(score < 14 ){
      return "5"
    } else if(score < 16 ){
      return "6"
    } else if(score < 18 ){
      return "7"
    } else if(score < 21){
      return "8"
    } else {
      return "0"
    }
    
  }

  setPokemonSprite(pokemon:String = this.favPokemon) {
    if(pokemon){
        this.pokeapiService.getSprite(this.favPokemon.toLowerCase()).subscribe(sprite => this.favPokemonSprite = sprite);
    } else {
      console.log("Pokemon sprite existerar inte")
    }
  }

  onClickBack() {
    this.location.back()
  }
  
  onClickTop() {
    window.scroll({
      top: 0,
      behavior: 'smooth'
    });
  }

}
