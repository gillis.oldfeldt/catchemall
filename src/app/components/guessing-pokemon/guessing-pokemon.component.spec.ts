import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuessingPokemonComponent } from './guessing-pokemon.component';

describe('GuessingPokemonComponent', () => {
  let component: GuessingPokemonComponent;
  let fixture: ComponentFixture<GuessingPokemonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuessingPokemonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GuessingPokemonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
