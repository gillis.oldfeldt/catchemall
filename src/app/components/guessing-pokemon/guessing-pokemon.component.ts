import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalstorageService } from 'src/app/services/localstorage.service';
import { PokeapiService } from 'src/app/services/pokeapi.service';
import { PokedexComponent } from '../pokedex/pokedex.component';
import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-guessing-pokemon',
  templateUrl: './guessing-pokemon.component.html',
  styleUrls: ['./guessing-pokemon.component.css']
})
export class GuessingPokemonComponent implements OnInit {

  pokemon$!: Observable<String>
  sprite$!: Observable<String>
  pokemonType$!: Observable<any>
  pokemon!: String
  poketext: String = ""
  noTypeHint: Boolean = true
  hideGuessButton = false
  correctGuess: Boolean = false
  correctGuesses: string = "No correct guesses yet"
  @Input() gen?: number
  @Output() guessPokemon = new EventEmitter()

  constructor(
    private pokeapiService: PokeapiService, 
    private localstorageService: LocalstorageService,
    private pokedexComponent: PokedexComponent) { }

  myControl = new FormControl();
  options: string[] = this.localstorageService.getPokeList();
  filteredOptions!: Observable<string[]>;

  ngOnInit(): void {
    this.updatePokemon()
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }

  onClickGuess() {
    this.correctGuess = (this.poketext.toLowerCase() == this.pokemon)
    this.hideGuessButton = false
    if (this.correctGuess) {
      this.localstorageService.setSeenPokedex(this.pokemon)
      if (this.correctGuesses == "No correct guesses yet") this.correctGuesses = "" + this.poketext
      else this.correctGuesses = this.correctGuesses + ", " + this.poketext
    }
  }

  onClickNext() {
    this.guessPokemon.emit({ noTypeHint: (this.correctGuess ? this.noTypeHint: false), pokemon: this.correctGuess })
    this.updatePokemon()
  }
  onClickTypingHint(): void {
    this.noTypeHint = false
  }

  updatePokemon(): void {
    this.hideGuessButton = true
    this.noTypeHint = true
    this.pokemon$ = this.gen ? this.pokeapiService.getRandomPokemonGen(this.gen) : this.pokeapiService.getRandomPokemon()
    this.poketext = ""


    this.pokemon$.subscribe(
      pokemon => {
        this.pokemon = pokemon;
        this.sprite$ = this.pokeapiService.getSprite(pokemon)
        this.pokemonType$ = this.pokeapiService.getPokemonType(pokemon)
      })
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }


}
