import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameStarterComponent } from './components/game-starter/game-starter.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { PokemonDetailComponent } from './components/pokemon-detail/pokemon-detail.component';
import { ProfileComponent } from './components/profile/profile.component';

const routes: Routes = [
  { path: '', redirectTo: '/game', pathMatch: 'full' },
  { path: 'pokedex', component: PokedexComponent },
  { path: 'pokemon/:name', component: PokemonDetailComponent },
  { path: 'game', component: GameStarterComponent },
  { path: 'profile', component: ProfileComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
